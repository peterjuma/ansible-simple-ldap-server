# ansible-simple-ldap-server

LDAP stands for Lightweight Directory Access Protocol. It’s an open protocol for accessing and maintaining distributed directory information services over an IP network (source Wikipedia).

The installation procedure has been automated using ansible whose manual steps can be found at [Certdepot](https://www.certdepot.net/rhel7)

# Usage

Customize the variables to suit your environment

 - ldapserver.yaml 
 - ldap/vars/main.yaml
 
# Post-Installation Checks

*Test the configuration with the test user called ldapuser01:*

`$ ldapsearch -x cn=ldapuser01 -b dc=example,dc=com`

The output should look something similar to this:

```
# extended LDIF
#
# LDAPv3
# base <dc=example,dc=com> with scope subtree
# filter: cn=ldapuser01
# requesting: ALL
#

# ldapuser01, Group, example.com
dn: cn=ldapuser01,ou=Group,dc=example,dc=com
objectClass: posixGroup
objectClass: top
cn: ldapuser01
userPassword:: e2NyeXB0fXg=
gidNumber: 1001

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
```


# Add a new LDAP user 

Create a LDIF file for New User 

But first, generate a password SHA for the user and replace it in the file.
For example, to generate a password has for pass123:

```
$ slappasswd -s pass123
{SSHA}HBsPbzRg+I5ax9vy4tpcfB3Z7mkfRYDa
```

ldapuser.ldiff 

```
dn: uid=ldapuser02,ou=People,dc=example,dc=com
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: ldapuser02
*userPassword*: {SSHA}HBsPbzRg+I5ax9vy4tpcfB3Z7mkfRYDa
loginShell: /bin/bash
uidNumber: 1010
gidNumber: 1010
homeDirectory: /home/guests/ldapuser02

dn: cn=ldapuser02,ou=Group,dc=example,dc=com
objectClass: posixGroup
cn: ldapuser02
gidNumber: 1010
memberUid: ldapuser02
```

# Add the LDAP User using ldapadd

`$ ldapadd -x -W -D 'cn=Manager,dc=example,dc=com' -H ldap://localhost -f ldapuser.ldiff`

```
Enter LDAP Password: 
adding new entry "uid=ldapuser02,ou=People,dc=example,dc=com"

adding new entry "cn=ldapuser02,ou=Group,dc=example,dc=com"
```

Note: Use the LDAP root password defined in `ldap/vars/main.yaml` file

Once again, to confirm the user configurations OK, run:

`$ ldapsearch -x cn=ldapuser02 -b dc=example,dc=com`  

```
# extended LDIF
#
# LDAPv3
# base <dc=example,dc=com> with scope subtree
# filter: cn=ldapuser02
# requesting: ALL
#

# ldapuser02, People, example.com
dn: uid=ldapuser02,ou=People,dc=example,dc=com
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: ldapuser02
userPassword:: e1NTSEF9SEJzUGJ6UmcrSTVheDl2eTR0cGNmQjNaN21rZlJZRGE=
loginShell: /bin/bash
uidNumber: 1011
gidNumber: 1011
homeDirectory: /home/guests/ldapuser02
uid: ldapuser02

# ldapuser02, Group, example.com
dn: cn=ldapuser02,ou=Group,dc=example,dc=com
objectClass: posixGroup
cn: Cent
cn: ldapuser02
gidNumber: 1011
memberUid: ldapuser02

# search result
search: 2
result: 0 Success

# numResponses: 3
# numEntries: 2
```


